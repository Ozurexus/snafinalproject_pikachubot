from django.contrib import admin
from .models import Theme, LinkToPicture, FavLocation, User, Schedule


@admin.register(Theme)
class ThemeAdmin(admin.ModelAdmin):
    pass


@admin.register(LinkToPicture)
class LinkToPictureAdmin(admin.ModelAdmin):
    pass


@admin.register(FavLocation)
class FavLocationAdmin(admin.ModelAdmin):
    pass


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    pass


@admin.register(Schedule)
class ScheduleAdmin(admin.ModelAdmin):
    pass
