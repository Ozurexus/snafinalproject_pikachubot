FROM python:3
ENV PYTHONUNBUFFERED 1

RUN mkdir -p /usr/src/SnaFinal
WORKDIR /usr/src/SnaFinal
COPY ./snaproject/requirements.txt /usr/src/SnaFinal
COPY ./snaproject /usr/src/SnaFinal
RUN pip install --no-cache-dir -r requirements.txt
